import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    utype: ''
  },
  mutations: {
    changeType (state, utype) {
      state.utype = utype
    }
  },
  getters: {
    utype: state => state.utype
  }
})
