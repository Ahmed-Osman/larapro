import Vue from 'vue'
import { store } from './store/store'
import VueRouter from 'vue-router'
import FullCalendar from 'vue-full-calendar'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)
Vue.use(Buefy)
Vue.use(FullCalendar)
Vue.use(VueRouter)
Vue.use(require('vue-moment'))
Vue.axios.defaults.baseURL = document.head.querySelector('meta[name="api-base-url"]').content
// Vue.axios.defaults.baseURL = 'http://localhost:8000/api'

import App from './components/App'
import Login from './components/authcomponents/Login'
import Register from './components/authcomponents/Register'
import Patient from './components/patientcomponents/Patient'
import PatientProfile from './components/patientcomponents/PatientProfile'
import BookDoctor from './components/patientcomponents/BookDoctor'
import Doctor from './components/doctorcomponents/Doctor'
import DoctorProfile from './components/doctorcomponents/DoctorProfile'
import AllBookings from './components/doctorcomponents/AllBookings'
import MyClincs from './components/doctorcomponents/MyClincs'
import NoAuth from './components/authcomponents/NotAuth'

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    props: { title: 'Welcome to DocBook' },
    meta: { authRequired: false }
  },
  {
    path: '/noauth',
    name: 'noauth',
    component: NoAuth
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    meta: { authRequired: false }
  },
  {
    path: '/patient',
    name: 'patient',
    component: Patient,
    meta: {
      auth: true,
      authRequired: true,
      role: 'pat'
    },
    children: [
      {
        path: 'profile',
        name: 'profile',
        component: PatientProfile,
        meta: {
          auth: true,
          authRequired: true,
          role: 'pat'
        },
        props: {
          patient: {
            name: 'Ali Hassan',
            email: 'Alihassan@gmail.com',
            number: '+966 56 625 7811'
          }
        }
      },
      {
        path: 'book-doctor',
        name: 'bookdoctor',
        component: BookDoctor,
        meta: {
          auth: true,
          authRequired: true,
          role: 'pat'
        }
      }
    ]
  },
  {
    path: '/doctor',
    name: 'doctor',
    component: Doctor,
    children: [
      {
        path: 'my-profile',
        name: 'myprofile',
        component: DoctorProfile
      },
      {
        path: 'all-bookings',
        name: 'allbookings',
        component: AllBookings,
        meta: {
          auth: true,
          authRequired: true,
          role: 'doc'
        }
      },
      {
        path: 'myclincs',
        name: 'myclincs',
        component: MyClincs
      }
    ]
  },
  { path: '*', redirect: '/home' },
  { path: '/', redirect: '/home' }
]

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

// set Vue router for the driver
Vue.router = router

Vue.use(require('@websanova/vue-auth'), {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  rolesVar: 'role'
})

router.beforeEach((to, from, next) => {
  // console.log('check where to go')
  console.log(to)

  const publicPages = ['/login', '/register']
  const isPublic = publicPages.includes(to.path)

  // check if the user looged in patient or doctor
  // and then action
  // console.log(Vue.auth.user())
  // console.log(Vue.auth.user().name)
  // console.log('"up n down"')

  console.log(' Meta Object')
  console.log(to.matched[0].meta)
  let role = ''

  console.log('User Object')
  console.log(Vue.auth.user()[0])
  if (Vue.auth.user()[0]) {
    console.log('logged user role')
    console.log(Vue.auth.user()[0].role)
    role = Vue.auth.user()[0].role
  }

  // console.log('role ' + role)
  // console.log('role ' + Vue.auth.user.role)
  // if not logged and visiting public ( login / register )
  if (isPublic && role === '') {
    return next()
  }
  // check if the link need authorization then validate the role visiting
  // rout meta extraction
  const m = to.matched[0].meta
  console.log(m)
  console.log('m Role ' + m.role)
  if (Vue.auth.user()[0]) {
    console.log('logged user role')
    console.log(Vue.auth.user()[0].role)
    role = Vue.auth.user()[0].role
  }
  if (m.authRequired === true) {
    // check if the logged user doesn't match the role link
    if (m.role !== role) {
      return next('/noauth')
    }
  } else if (m.authRequired === false) {
    // still logged in and trying to visit login or register
    // if role doc then redirect to his land and same for patient
    if (role === 'doc') {
      next({ name: 'allbookings' })
    } else if (role === 'pat') {
      next({ name: 'bookdoctor' })
    }
  }
  next()
})

const app = new Vue({
  el: '#app',
  store,
  components: { App },
  router
})
