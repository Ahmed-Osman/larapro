<?php

use Illuminate\Http\Request;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClincController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post(‘auth/register’, ‘AuthController@register’);

Route::post('auth/register', 'AuthController@register');

Route::post('auth/login', 'AuthController@login');
// Route::get('auth/logout', 'AuthController@logout');




//Auth
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@user');
});

Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'AuthController@refresh');
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::post('auth/logout', 'AuthController@logout');
});

//clinc apis
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::post('/myclincs', 'ClincController@new');
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/allmyclincs', 'ClincController@getAll');
});


// Appointment Doctor
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::put('/docAccept', 'AppointmentController@acceptAppoint');
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::put('/docRefuse', 'AppointmentController@refuseAppoint');
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/allDocReserves/{id}', 'AppointmentController@getAllDoctorBookings');
});

//Appointment Patient
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::post('/bookAppointment', 'AppointmentController@bookAppointment');
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::delete('/deleteMyAppointment/{id}', 'AppointmentController@deleteMyAppointment');
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/allReserves/{id}', 'AppointmentController@getAllReserves');
});

//Patient
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/allDocs', 'PatientController@getAllDoctors');
});