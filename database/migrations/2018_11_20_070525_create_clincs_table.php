<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClincsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clincs', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('desc');
            $table->string('name',100)->unique();
            $table->integer('seat');
            $table->string('speciality', 100);
            $table->time('o_time');
            $table->time('c_time');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clincs');
    }
}
