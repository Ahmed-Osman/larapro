<?php

namespace App\Http\Controllers;

use App\User;
use App\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Clinc;

class AppointmentController extends Controller
{
    
    public function getAllReserves(Request $request, $id){
        Log::info($request);
        $appoints = Appointment::with('clinc')->where('user_id',$id)->where('accept', '!=', 'refused')->get();
        return response([
            'status' => 'success',
            'appoints' => $appoints
        ],200);
    }

    //Doctor
    public function acceptAppoint(Request $request){
        Log::info($request);
        $app = Appointment::where('id', $request->id)->update(['accept'=>'accepted']);
        return response([
            'status' => 'success',
            'data' => $app
        ]);
    }

    //doctor
    public function refuseAppoint(Request $request){
        Log::info($request);
        $app = Appointment::where('id', $request->id)->update(['accept'=>'refused']);
        return response([
            'status' => 'success',
            'data' => $app
        ]);
    }

    //deleting from client
    public function deleteMyAppointment(Request $request, $id){
        Log::info($request);
        $app = Appointment::find($id)->delete();
        return response([
            'status' => 'success',
            'data' => $app
        ]);
    }

    //Doctor
    public function getAllDoctorBookings(Request $request, $id){
        //Tested Tinker
        $appoints = Appointment::with(['user.detail', 'clinc' => function($query) use($id){ 
            $query->where('user_id', 'like', $id);
        }])->where('accept', '!=', 'refused')->get();
        
        //filtier out all the null clincs means doesn't belong to the dotocr requesting.
        $col = $appoints->reject(function ($appoints) {
            return $appoints->clinc === null;
        })
        ->map(function ($appoints) {
            return $appoints;
        });

        return response([
            'status' => 'success',
            'appoints' => $col
        ], 200);
    }

    public function bookAppointment(Request $request){
        $user = User::find($request->user()->id);


        //check this date in clinc
        $appCounted = Appointment::where('clinc_id', $request->clinc_id)
            ->whereDate('reserve_date', '=', date($request->reserve_date))->where('accept', '!=', 'refused')->count();
        $clinc = Clinc::find($request->clinc_id);

        if ($clinc->seat <= $appCounted){
            //return you won't be able to reserve that date
            return response([
                'status' => 'failed',
                'message' => 'The clinc ai\'nt available this day'
            ]);
        }
        
        //if pass through if then reserve an appointment that day.

        $appointment = new Appointment;
        $appointment->user_id = $request->user()->id;
        $appointment->clinc_id= $request->clinc_id;
        $appointment->reserve_date= $request->reserve_date;
        $appointment->problem= $request->problem_description;
        $appointment->save();
        $inserted = $appointment->id;
        //return the created appointment 
        $data= Appointment::with('clinc')->where('id', $inserted)->get();
        return response([
            'status' => 'success',
            'data'=> $data
        ], 200);
    }
}
