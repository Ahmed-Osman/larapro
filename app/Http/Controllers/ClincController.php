<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Clinc;
use App\User;
use App\Appointment;

class ClincController extends Controller
{

    public function new(Request $request){
        $id =$request->user();
        Log::info($id);
        $clinc = new Clinc;
        $clinc->desc = $request->desc;
        $clinc->seat = $request->seat;
        $clinc->o_time = $request->o_time;
        $clinc->c_time = $request->c_time;
        $clinc->speciality = $request->speciality;
        $clinc->user_id = $id->id;
        $clinc->name = $request->name;
        $clincIns = $clinc->save();
        
        return response([
            'status' => 'success',
            'data' => $clinc
        ]);
    }

    //All Doc Clinic
    public function getAll(Request $request){
        // $user = $request->user();
        $user= User::find($request->user()->id);
        $clincs= $user->clincs()->get();
        return response($clincs, 200);
    }

}
