<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Clinc;
use App\User;

class PatientController extends Controller
{
    //
    public function getAllDoctors(){
        $docs = User::with('clincs')->where('role','doc')->get();
        return response([
            'status' => 'success',
            'data' => $docs
        ],200);
    }
}
