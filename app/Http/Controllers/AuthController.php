<?php

namespace App\Http\Controllers;
use Log;
use App\User;
use App\PDetail;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterFormRequest;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        // Craete rule to validate with if email is duplicated
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users'
        ]);

        //return Bad Request if duplicated mention the problem
        if ($validator->fails()) {
            return response([
                'status' => 'failed',
                'message' => 'You cannot use this email'
            ], 400);
        }

        // create user
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        $user->save();

        // check his role if patient insert the remaining fields.
        if ($request->role == "pat") {
            $detail = new PDetail;
            $detail->mobile = $request->mobile;
            $detail->desc = $request->desc;
            $user->detail()->save($detail);
        }
        
        // return inserted object with 200 success
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function login (Request $request)
    {
        $cred = $request->only('email', 'password');
        
        if(! $token = JWTAuth::attempt($cred)){
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'invalid credentials'
            ], 400);
            }
        return response([
            'status' => 'success'
        ])->header('Authorization', $token);
    }

    public function user(Request $request){
        $user = User::find(Auth::user()->id);
        if ($user->role == "pat"){
            $user = User::with('detail')->where('role','pat')->where('id', Auth::user()->id)->get();
        }
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function refresh(){
        return response([
            'status' => 'success'
        ]);
    }

    public function logout(){
        JWTAuth::invalidate();
        return response([
            'status' => 'success',
            'msg' => 'logged out succ',
        ], 200);
    }
}