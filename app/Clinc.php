<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinc extends Model
{
    // mass assignable
    protected $fillable = [
        'desc', 'speciality', 'seats', 'o_time', 'c_time'
    ];

    //
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }
}